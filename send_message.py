import zmq
import datetime
import sys 


if len(sys.argv) < 2:
    print("Must provoide a message argument")
    sys.exit(1)

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.connect("tcp://localhost:5559")

socket.send_string(sys.argv[1])

# If uncommented, will wait for server response (blocking) kinda like HTTP
#after = datetime.datetime.now()

#print("Time taken (ms): " + str((after - before).total_seconds() * 1000)) 