#!/usr/bin/python
# -*- coding: UTF-8 -*-

import zmq


def main():

    context = zmq.Context()

    # Socket facing producers
    frontend = context.socket(zmq.XPUB)
    frontend.bind("tcp://*:5560")

    # Socket facing consumers
    backend = context.socket(zmq.PULL)
    backend.bind("tcp://*:5559")

    # Publish everything to a capture socket
    capture = context.socket(zmq.PUB)
    capture.bind("tcp://*:5561")

    while True:
        msg = backend.recv() 
        print(msg)
        #backend.send_string("OK")
        frontend.send(msg)        

    #zmq.proxy(frontend, backend, capture)

    # We never get here…
    frontend.close()
    backend.close()
    context.term()

if __name__ == "__main__":
    main()