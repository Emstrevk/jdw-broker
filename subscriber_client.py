import zmq 


# Example subscriber for messages passed into the main server 
# Note the setsockopt which defines a filter and the decode/split handling for recv() 

context = zmq.Context()


subscribe = context.socket(zmq.SUB)
subscribe.connect("tcp://localhost:5560")

# Filter, default filters everything (ie no messages received)
# Essentially a "begins_with()" check for the received message
subscribe.setsockopt(zmq.SUBSCRIBE, b'JDW.REQ.NOTE.PLAY::')

while True:
    message = subscribe.recv()
    print(message.decode('utf-8').split("::")[1])
