#!/usr/bin/python
# -*- coding: UTF-8 -*-

import zmq


print("DEBUG: Outputting all router traffic")

context = zmq.Context()

# Socket facing debug capture
frontend = context.socket(zmq.SUB)
frontend.connect("tcp://localhost:5561")
frontend.setsockopt(zmq.SUBSCRIBE, b'')

while True:
    print(frontend.recv().decode('utf-8'))